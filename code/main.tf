terraform {
  required_providers {
    linode = {
      source  = "linode/linode"
      version = "~> 1.0"
    }
  }
}

provider "linode" {
  token = var.linode_api_token
}


variable "linode_api_token" {
    description = "Your Linode API Personal Access Token. (required)"
    sensitive   = true
}

resource "linode_instance" "debian_vm2" {
  label            = "docker-host-debian"
  image            = "linode/debian10"
  region           = "us-east"
  type             = "g6-nanode-1"
  root_pass        = "n8M&h3!VzC@qY^7t"
  private_ip       = true

  tags = ["taller"]
}
